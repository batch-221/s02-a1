#1 create database

CREATE DATABASE blog_db;

#2 use database

USE blog_db;


#3 CREATE TABLES FOR USERS

CREATE TABLE users (id INT NOT NULL AUTO_INCREMENT, email VARCHAR(100) NOT NULL, password VARCHAR(300) NOT NULL, datetime_created DATETIME NOT NULL,
    PRIMARY KEY (id)
);


#4 CREATE TABLE FOR posts

CREATE TABLE posts (
	id int not null AUTO_INCREMENT,
	user_id int not null,
	title varchar(500) not null,
	content VARCHAR(5000) not null,
	datetime_posted DATETIME not null,
	PRIMARY key(id),
	CONSTRAINT fk_posts_user_id
		FOREIGN key (user_id) REFERENCES users(id)
		ON UPDATE CASCADE
		ON DELETE RESTRICT
);

#5 CREATE TABLE post_likes

CREATE TABLE post_likes (
	id int not null AUTO_INCREMENT,
	post_id int not null,
	user_id int not null,
	datetime_liked DATETIME not null,
	PRIMARY key (id),
	CONSTRAINT fk_post_likes_user_id
        FOREIGN KEY (user_id) REFERENCES users(id)
        ON UPDATE CASCADE
        ON DELETE RESTRICT,

    CONSTRAINT fk_post_likes_post_id
        FOREIGN KEY (post_id) REFERENCES posts(id)
        ON UPDATE CASCADE
        ON DELETE RESTRICT
);

#6 CREATE TABLE post_comments

CREATE TABLE post_comments (
	id int not null AUTO_INCREMENT,
	post_id int not null,
	user_id int not null,
	content varchar(5000),
	datetime_commented DATETIME not null,
	PRIMARY key (id),
	CONSTRAINT fk_post_comments_user_id
        FOREIGN KEY (user_id) REFERENCES users(id)
        ON UPDATE CASCADE
        ON DELETE RESTRICT,
    CONSTRAINT fk_post_comments_post_id
        FOREIGN KEY (post_id) REFERENCES posts(id)
        ON UPDATE CASCADE
        ON DELETE RESTRICT

);
